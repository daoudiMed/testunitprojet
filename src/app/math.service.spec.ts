import { TestBed } from '@angular/core/testing';

import { MathService } from './math.service' ;
  describe('MyServiceService' , () => {
  it('La méthode incrementCount doit incrémenter la propriété count' , () => {
  // Instanciation du MathService
  let mathService = new MathService();
  // Premier incrément
  mathService.incrementCount();
  expect(mathService.count).toEqual(1);
  // Second incrément
  mathService.incrementCount();
  expect(mathService.count).toEqual(2);
  });
});